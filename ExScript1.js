// JavaScript source code
$('#budgetCal').click(() => {
    var budget;

    budget = Math.floor($('#budget').val());

    var yourBudget = JSON.parse(localStorage.getItem('budget'));

    if (budget.money != '') {
        if (yourBudget == null) {
            var yourBudget = [];
        }
        else {
            yourBudget.splice(0, 1)
        }
        yourBudget.push(budget);
        
        localStorage.setItem('budget', JSON.stringify(yourBudget))
        document.getElementById("budget").disabled = true;
        document.getElementById("budgetCal").disabled = true;
        displayBudget();
    }
    else {
        document.getElementById("budget").value = '';
        swal("Error", "Please Fill in the Boxes", "error");
    }
});

$('#addExpense').click(() => {
    var expense = {};
    
    expense.name = $('#nameExpense').val();
    expense.amount = Math.floor($('#amountExpense').val());

    var yourBudget = JSON.parse(localStorage.getItem('budget'))

    if (yourBudget == null) {
        swal("Error", "Please Fill in the Box Above", "error")
    }
    else {
        if (yourBudget < expense.amount) {
            swal("Error", "Please Enter Your Expense Smaller Than Your Budget", "error")
        }
        else {
            var yourExpense = JSON.parse(localStorage.getItem('expense'));
    
            if (expense.name != '' || expense.amount != '') {
                if (yourExpense == null) {
                    var yourExpense = []
                }
                
                yourExpense.push(expense);
                localStorage.setItem('expense', JSON.stringify(yourExpense))
                document.getElementById("nameExpense").value = '';
                document.getElementById("amountExpense").value = '';
                displayChart();
                displayTable();

            }
            else {
                swal("Error", "Please Fill in the Boxes", "error")
                document.getElementById("nameExpense").value = '';
                document.getElementById("amountExpense").value = '';
            }
        }
    }
});

function displayBudget() {
    var yourBudget = JSON.parse(localStorage.getItem('budget'))
    $('#showBudget').html("$ " + yourBudget.toString())
    $('#showBalance').html("$ " + (yourBudget).toString())

}

function displayChart() {
    var yourBudget = JSON.parse(localStorage.getItem('budget'))
    var yourExpense = JSON.parse(localStorage.getItem('expense'));
    var totExpense = 0;
    for (var i = 0; i < yourExpense.length; i++) {

        totExpense += yourExpense[i].amount;
    }
    $('#showExpenses').html("$ " + totExpense.toString())
    $('#showBalance').html("$ " + (yourBudget - totExpense).toString())
    localStorage.setItem('totalExpense', JSON.stringify(totExpense))
    localStorage.setItem('Balance', JSON.stringify((yourBudget - totExpense)))
}

function displayTable() {
    var yourExpense = JSON.parse(localStorage.getItem('expense'));

    var table = `<tbody>
                 <tr>\
                    <th><center><b>Expense Title</b></center></th>\
                    <th><center><b>Expense Value</b></center></th>\
                    <th><center><b>Actions</b></center></th>\
                    </tr>\
                   < tr >`;

    for (var i = 0; i < yourExpense.length; i++) {
        table += ` 
                    <td><center>${yourExpense[i].name}</center></td>\
                    <td><center>${yourExpense[i].amount}</center></td>\
                    <td><center><button class="btn btn-danger" onclick="trash(${i})"><i class="fa fa-trash"></i></i></button>
                    <button onclick="edit(${i})" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button></center></td>\ 
                    </tr >`
    }
    table += '</tbody>'

    $('#showTable').html(table)
}

function trash(i) {
    var yourExpense = JSON.parse(localStorage.getItem('expense'));
    yourExpense.splice(i, 1)
    localStorage.setItem('expense', JSON.stringify(yourExpense));
    displayTable();
    displayChart();
}

function edit(i) {
    var yourExpense = JSON.parse(localStorage.getItem('expense'));
    $('#newEx').val(yourExpense[i].name);
    $('#newNum').val(yourExpense[i].amount);
    $('#hid').val(i);

}

function editclick() {
    var expense = {};
    var yourExpense = JSON.parse(localStorage.getItem('expense'));
    expense.name = $('#newEx').val()
    expense.amount = Math.floor($('#newNum').val())
    var number = $('#hid').val()
    yourExpense.splice(number, 1)
    yourExpense.push(expense);
    localStorage.setItem('expense', JSON.stringify(yourExpense));
    displayTable();
    displayChart();
}